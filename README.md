Gamma Ray 軟體工作室
=====
`歡迎來到 Gamma Ray 軟體工作室`

這裡整理過去學習程式設計時，記錄下來的各種知識技術筆記，

並且會著重在軟體設計的**架構**，以及軟體開發的**觀念**和**經驗**上。

協助程式設計師擁有嚴謹的編程思維，發揮軟體服務可以帶來的競爭優勢。

Link
------
### Blogger
<https://gamma-ray-studio.blogspot.com/>

### Youtube
<https://www.youtube.com/user/rhxs020>



